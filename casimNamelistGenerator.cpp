//(c) Phil Rosenberg, University of Leeds, p.d.rosenberg@leeds.ac.uk
//If you use this code, please at least drop me an email to mention
//so and say thanks :-) It's always good to know who uses my code.
//Plus it means I can keep you updated if I find a bug or make an
//improvement.

#include<fstream>
#include<vector>
#include<iomanip>
#include<cmath>

//Comment this out if you want to specify the lognormal mode masses
//or leave it uncommented if you wish to specify the lognormal mode and width
//and use these to calculate the masses
//#define USE_MODE_AND_WIDTH

const std::vector<double> etaOnModelLevels{ 0.1250000E-03,   0.5416666E-03,   0.1125000E-02,   0.1875000E-02,
0.2791667E-02,   0.3875000E-02,   0.5125000E-02,   0.6541667E-02,   0.8125000E-02,
0.9875000E-02,   0.1179167E-01,   0.1387500E-01,   0.1612500E-01,   0.1854167E-01,
0.2112500E-01,   0.2387500E-01,   0.2679167E-01,   0.2987500E-01,   0.3312500E-01,
0.3654167E-01,   0.4012500E-01,   0.4387500E-01,   0.4779167E-01,   0.5187500E-01,
0.5612501E-01,   0.6054167E-01,   0.6512500E-01,   0.6987500E-01,   0.7479167E-01,
0.7987500E-01,   0.8512500E-01,   0.9054167E-01,   0.9612500E-01,   0.1018750E+00,
0.1077917E+00,   0.1138750E+00,   0.1201250E+00,   0.1265417E+00,   0.1331250E+00,
0.1398750E+00,   0.1467917E+00,   0.1538752E+00,   0.1611287E+00,   0.1685623E+00,
0.1761954E+00,   0.1840590E+00,   0.1921980E+00,   0.2006732E+00,   0.2095645E+00,
0.2189729E+00,   0.2290236E+00,   0.2398690E+00,   0.2516917E+00,   0.2647077E+00,
0.2791699E+00,   0.2953717E+00,   0.3136506E+00,   0.3343919E+00,   0.3580330E+00,
0.3850676E+00,   0.4160496E+00,   0.4515977E+00,   0.4924007E+00,   0.5392213E+00,
0.5929016E+00,   0.6543679E+00,   0.7246365E+00,   0.8048183E+00,   0.8961251E+00,
0.1000000E+01 };
const double modelTopHeight = 40000.0;
const size_t firstConstantRhoLevel = 61; //note one less than in the vert_levs file as C++ has 0 indexed arrays.

double heightFromEta(double eta, double modelTopHeight, double firstConstantRhoLevel)
{
	return eta * modelTopHeight;
}

void writeVariable(std::fstream &fout, size_t id, double startValue, double boundaryLayerHeight, double atmosphereExponentialDecayRate, double aboveBoundaryLayerAerosolDecayRate, bool finalVariable)
{
	fout << id << "\n";
	for (size_t i = 0; i < etaOnModelLevels.size(); ++i)
	{
		double height = heightFromEta(etaOnModelLevels[i], modelTopHeight, firstConstantRhoLevel);
		double conc = startValue * std::exp(-height * atmosphereExponentialDecayRate);
		if (height > boundaryLayerHeight)
			conc *= std::exp(-(height - boundaryLayerHeight) * aboveBoundaryLayerAerosolDecayRate);
		fout << "      " << std::scientific << std::setprecision(8) << conc;
		if (!finalVariable)
			fout << "\n";
	}
}

int main()
{
	std::string fileName = "accum_1000cm-3_exp.nml";

	//number concentrations are in m-3, so 1e8 is 100 cm-3
	//mass concentrations are in kg m-3. In the initial
	//Cope experiment accumulation mode mass was set to 
	//1.2e-9 and number to 1e8, so keep the ratio of these
	//numbers the same if you wish to keep the mean size the
	//same
	const double fineSurfaceNumber = 0.0;
	const double accumulationSurfaceNumber = 1.00000e+08*10;
	const double coarseSurfaceNumber = 0.0;
	const double accumulationInsolSurfaceNumber = 46631.1;
	const double coarseInsolSurfaceNumber = 0.0;
#ifndef USE_MODE_AND_WIDTH
	const double fineSurfaceMass = 0.0;
	const double accumulationSurfaceMass = 1.20000e-09*10;
	const double coarseSurfaceMass = 0.0;
	const double accumulationInsolSurfaceMass = 5.88700e-07;
	const double coarseInsolSurfaceMass = 0.0;
#else
	/*const double density = 1e3;

	//Specify the mode diameter
	const double fineMode = 3.00000e+08;
	const double accumulationMode = 3.00000e+08;
	const double coarseMode = 3.60000e-07;
	const double accumulationInsolMode = 46631.1;
	const double coarseInsolMode = 5.88700e-07;

	//this is the sigma parameter on the lognormal wikipedia page, i.e. the standard deviation of ln(diameter)
	const double fineWidth = 3.00000e+08;
	const double accumulationWidth = 3.00000e+08;
	const double coarseWidth = 3.60000e-07;
	const double accumulationInsolWidth = 46631.1;
	const double coarseInsolWidth = 5.88700e-07;

	const double fineLocation = std::log(fineMode) - fineWidth*fineWidth;
	const double accumulationLocation = std::log(accumulationMode) - accumulationWidth*accumulationWidth;
	const double coarseLocation = std::log(coarseMode) - coarseWidth*coarseWidth;
	const double accumulationInsolLocation = std::log(accumulationInsolMode) - accumulationInsolWidth*accumulationInsolWidth;
	const double coarseInsolLocation = std::log(coarseInsolMode) - coarseInsolWidth*coarseInsolWidth;

	const double accumulationSurfaceMass = ;*/
#endif

	const double gravity = 9.81;
	const double molarMass = 0.02896;
	const double gasConstant = 8.314;
	const double temperature = 288.15;

	double boundaryLayerHeight = 5000.0;
	double atmosphereExponentialDecayRate = gravity * molarMass / gasConstant / temperature;
	double aboveBoundaryLayerAerosolDecayRate = 1.0 / 1000.0;

	std::fstream fout;
	fout.open(fileName.c_str(), std::ios::out);
	writeVariable(fout, 33001, fineSurfaceMass, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33002, fineSurfaceNumber, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33003, accumulationSurfaceMass, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33004, accumulationSurfaceNumber, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33005, coarseSurfaceMass, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33006, coarseSurfaceNumber, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33007, accumulationInsolSurfaceMass, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33008, accumulationInsolSurfaceNumber, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33009, coarseInsolSurfaceMass, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	writeVariable(fout, 33010, coarseInsolSurfaceNumber, boundaryLayerHeight, atmosphereExponentialDecayRate, aboveBoundaryLayerAerosolDecayRate, false);
	fout.close();
	return 0;
}
