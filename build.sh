echo
echo
echo "Checking GCC version"
echo
gcc --version
echo
echo "*******************************************"
echo "*WARNING                                  *"
echo "*If the version listed above is not 4.4 or*"
echo "*greater, then this code will not compile.*"
echo "*Try compiling on the postproc machine if *"
echo "*you see errors below, no output below    *"
echo "means all was fine                        *"
echo "*******************************************"

g++ -std=c++0x casimNamelistGenerator.cpp -o casimNamelistGenerator
